//
//  ALAssetsLibrary+Singleton.m
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import "ALAssetsLibrary+Singleton.h"

@implementation ALAssetsLibrary (Singleton)

static ALAssetsLibrary *sharedLibrary = nil;

+(ALAssetsLibrary *)sharedLibrary
{
	static dispatch_once_t pred;
	
	dispatch_once(&pred, ^{
		sharedLibrary = [[ALAssetsLibrary alloc] init];
	});
    
	return sharedLibrary;
}


@end
