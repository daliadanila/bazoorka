//
//  ViewController.m
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    CLLocationManager *locationManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	locationManager = [[CLLocationManager alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    [locationManager stopUpdatingLocation];
}

- (IBAction)photoAction:(id)sender
{
    UIActionSheet *popupQuery = [[UIActionSheet alloc] initWithTitle:@"Set your picture" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take photo", @"Choose picture", nil];
    popupQuery.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [popupQuery showInView:self.view];
}


# pragma mark UIActionSheetDelegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self takePhoto];
            break;
        case 1:
            [self choosePhoto];
            break;
        default:
            break;
    }
}

- (void)takePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate = (id)self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
    }
    
    // Setting the location manager's delegate
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    
    // show camera view
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)choosePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    
    imagePickerController.delegate = (id)self;
    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    
    // show library view
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info
{
    // Create our queue in order to push image view controller after the gps data is available
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    myQueue.name = @"My Super Queue";
    myQueue.maxConcurrentOperationCount = 1;
    
    // When choosing photo from Camera Roll
    if(picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
    {
        [myQueue addOperationWithBlock:^{
            ALAssetsLibrary *assetLibrary = [ALAssetsLibrary sharedLibrary];
            [assetLibrary assetForURL:[info objectForKey:@"UIImagePickerControllerReferenceURL"] resultBlock:^(ALAsset *asset)
             {
                 
                 // Get hold of main queue (main thread)
                 [[NSOperationQueue mainQueue] addOperationWithBlock: ^ {
                     ALAssetRepresentation *rep = [asset defaultRepresentation];
                     NSDictionary *metadata = [rep metadata];
                     NSMutableDictionary *GPSDictionary;
                     if([metadata objectForKey:@"{GPS}"])
                     {
                         NSString *tempGPS = [NSString stringWithFormat:@"%@",[metadata objectForKey:@"{GPS}"]];
                         
                         NSString *lat;
                         NSString *lon;
                         
                         // Getting the latitude
                         NSRange startRangeLat = [tempGPS rangeOfString:@"Latitude"];
                         NSRange endRange = [tempGPS rangeOfString:@";"];
                         
                         NSRange searchRangeLat = NSMakeRange(startRangeLat.location , endRange.location);
                         lat = [tempGPS substringWithRange:searchRangeLat];
                         
                         // Getting the longitude
                         NSRange startRangeLon = [tempGPS rangeOfString:@"Longitude"];
                         
                         NSRange searchRangeLon = NSMakeRange(startRangeLon.location , endRange.location);
                         lon = [tempGPS substringWithRange:searchRangeLon];
                         
                         
                         GPSDictionary = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          lat, @"Latitude",
                                          lon, @"Longitude",
                                          nil];
                         
                     }
                     else
                         GPSDictionary = nil;
                     
                     // close the picker
                     [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
                     
                     // prepare the next view
                     ImageViewViewController *imageView = [[ImageViewViewController alloc] initWithNibName:@"ImageViewViewController" bundle:nil];
                     if(GPSDictionary) imageView.GPSDictionary = GPSDictionary;
                     
                     // save image
                     UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
                     //NSData *data = UIImagePNGRepresentation(image);
                     //[data writeToFile:[self ourImagePath] atomically:YES];
                     [AppDelegate setImage: image];
                     
                     // go to next view
                     [self.navigationController pushViewController:imageView animated:YES];
                 }];
                 
             } failureBlock:^(NSError *err) {
                 NSLog(@"Error: %@",[err localizedDescription]);
             }];
        }];
    }
    
    // When taking the picture with camera
    else
    {
        if(picker.sourceType == UIImagePickerControllerSourceTypeCamera)
        {
            float lat = locationManager.location.coordinate.latitude;
            float lon = locationManager.location.coordinate.longitude;
            
            // Here's our dictionary with the gps data obtain using CLLocationManager
            NSDictionary *GPSDictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Latitude %@", [NSNumber numberWithFloat:lat]], @"Latitude",
                                                                                     [NSString stringWithFormat:@"Longitude %@", [NSNumber numberWithFloat:lon]], @"Longitude", nil];
            
            // close the picker
            [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
            
            // prepare the next view
            ImageViewViewController *imageView = [[ImageViewViewController alloc] initWithNibName:@"ImageViewViewController" bundle:nil];
            if(GPSDictionary) imageView.GPSDictionary = GPSDictionary;
            
            // save image
            UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//            NSData *data = UIImagePNGRepresentation(image);
//            [data writeToFile:[self ourImagePath] atomically:YES];
            [AppDelegate setImage: image];
            
            // go to next view
            [self.navigationController pushViewController:imageView animated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"This device doesn't have a camera.." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

- (NSString *)ourImagePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* fileNameExt = [NSString stringWithFormat:@"%@.%@", @"superImage", @"png"];
    
    return [documentsDirectory stringByAppendingPathComponent:fileNameExt];
}

@end
