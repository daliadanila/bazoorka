//
//  MapViewController.m
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void) viewDidAppear:(BOOL)animated
{
    NSString *stringPath = [[NSBundle mainBundle] pathForResource:@"story" ofType:@"mp3"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:stringPath];
    
    NSError * error = nil;
    playerRingtone = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    if(!playerRingtone)
        NSLog(@"Error %@",error);
    
    [playerRingtone setDelegate:self];
    playerRingtone.numberOfLoops = -1;
    [playerRingtone prepareToPlay];
    [playerRingtone play];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
