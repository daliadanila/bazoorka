//
//  ViewController.h
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import <CoreLocation/CoreLocation.h>
#import "ALAssetsLibrary+Singleton.h"
#import "ImageViewViewController.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController<UIImagePickerControllerDelegate, CLLocationManagerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) UIImage * image;

- (IBAction)photoAction:(id)sender;

@end
