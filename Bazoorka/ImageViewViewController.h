//
//  ImageViewViewController.h
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ImageIO/ImageIO.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "MapViewController.h"

@interface ImageViewViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (nonatomic, assign) CGFloat currentScale;
@property (nonatomic, strong) NSDictionary *GPSDictionary;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
- (IBAction)openMap:(id)sender;

- (IBAction)shareByEmail:(id)sender;
- (IBAction)shareOnTwitter:(id)sender;
- (IBAction)shareOnFacebook:(id)sender;

@end
