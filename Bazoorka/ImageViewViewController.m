//
//  ImageViewViewController.m
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import "ImageViewViewController.h"
#import "AppDelegate.h"

@interface ImageViewViewController ()
@end


@implementation ImageViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Loading the image
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString* fileNameExt = [NSString stringWithFormat:@"%@.%@", @"superImage", @"png"];
//    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:fileNameExt];
    //self.imageView.image = [UIImage imageWithContentsOfFile: savedImagePath];
    
    // load image from app delegate
    self.imageView.image = [AppDelegate getImage];
    
    // Adding gesture for zooming
    UIPinchGestureRecognizer  *pinch= [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(makePinch:)];
    [self.imageView addGestureRecognizer:pinch];
}


-(void)makePinch:(UIPinchGestureRecognizer*)pinch
{
    if(pinch.state == UIGestureRecognizerStateEnded)
    {
        self.currentScale = pinch.scale;
    }
    else if(pinch.state == UIGestureRecognizerStateBegan && self.currentScale != 0.0f)
    {
        pinch.scale = self.currentScale;
    }
    
    if(pinch.scale != NAN && pinch.scale != 0.0)
    {
        pinch.view.transform = CGAffineTransformMakeScale(pinch.scale, pinch.scale);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)openMap:(id)sender
{
    MapViewController *mapView = [[MapViewController alloc]initWithNibName:@"MapViewController" bundle:nil];
    
    [self.navigationController pushViewController:mapView animated:YES];
}

- (IBAction)shareByEmail:(id)sender
{
    if ( [MFMailComposeViewController canSendMail] )
    {
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        
        if (self.GPSDictionary)
        {
            NSString *lat = [self.GPSDictionary valueForKey:@"Latitude"];
            NSString *lon = [self.GPSDictionary valueForKey:@"Longitude"];
            [picker setMessageBody:[NSString stringWithFormat:@"Check this image made at location: %@ %@", lat, lon] isHTML: NO];
        }
        
        NSData *exportData = UIImageJPEGRepresentation(self.imageView.image, 1.0);
        [picker setSubject:@"Photo sent by Bazooka :)"];
        [picker addAttachmentData:exportData mimeType:@"image/png" fileName:@"Image.png"];
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please set your account for Email.." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)shareOnTwitter:(id)sender
{    
    SLComposeViewController *tweetComposer;
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        tweetComposer = [[SLComposeViewController alloc] init];
        tweetComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        if(self.GPSDictionary)
        {
            NSString *lat = [self.GPSDictionary valueForKey:@"Latitude"];
            NSString *lon = [self.GPSDictionary valueForKey:@"Longitude"];
            [tweetComposer setInitialText:[NSString stringWithFormat:@"Check this image made at location: %@ %@", lat,lon]];
        }
        
        [tweetComposer addImage:self.imageView.image];
        [tweetComposer setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            if (result == SLComposeViewControllerResultDone)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully tweeted!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
        }];
        
        [self presentViewController:tweetComposer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please set your account for Twitter.." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)shareOnFacebook:(id)sender
{    
    SLComposeViewController *facebookComposer;
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        facebookComposer = [[SLComposeViewController alloc] init];
        facebookComposer = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        if(self.GPSDictionary)
        {
            NSString *lat = [self.GPSDictionary valueForKey:@"Latitude"];
            NSString *lon = [self.GPSDictionary valueForKey:@"Longitude"];
            [facebookComposer setInitialText:[NSString stringWithFormat:@"Check this image made at location: %@ %@ ", lat, lon]];
        }
        [facebookComposer addImage:self.imageView.image];
        [facebookComposer setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            if (result == SLComposeViewControllerResultDone)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully posted on Facebook!" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                
                [alert show];
            }
        }];
        
        [self presentViewController:facebookComposer animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please set your account for Facebook.." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

// Notifies users about errors associated with the interface

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Email" message:NSLocalizedString(@"Email saved!", nil)  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
            break;
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Email" message:NSLocalizedString(@"Email sent!",nil) delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
            break;
        case MFMailComposeResultFailed:
            break;
            
        default:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message:@"Sending Failed - Unknown Error :-(" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
