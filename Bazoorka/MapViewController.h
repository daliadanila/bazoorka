//
//  MapViewController.h
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MapViewController : UIViewController <AVAudioPlayerDelegate>
{
    AVAudioPlayer *playerRingtone;
}
@end
