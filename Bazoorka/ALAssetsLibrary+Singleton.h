//
//  ALAssetsLibrary+Singleton.h
//  Bazoorka
//
//  Created by dalia on 7/30/13.
//  Copyright (c) 2013 p5. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

@interface ALAssetsLibrary (Singleton)


+(ALAssetsLibrary *)sharedLibrary;

@end
